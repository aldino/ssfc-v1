$(document).ready(function(){

	function _removeButton() {
		$('.display-thumb > .btn-remove').on('click', function(){
			var file = this.getAttribute('fileName');
			$.ajax({
				url 		: baseUrl+'post/delete_thumb/'+file,
    		type 		: "POST",
    		dataType: "json"
			}).done(function(r){
				var html =
          '<input type="file" id="choose-thumb" class="loading" style="display: none;">'
          +'<img src="'+baseUrl+'uploads/default/default-image.jpg">';
          $('.display-thumb').html(html);
          load();
			}).fail(function(){

			});
		});
	}

	function uploadButton() {
		$('#choose-thumb').on('change', function(){
			readUrl(this);
		});
		$('#banner').on('change', function() {
			uploadBanner(this);
		});
	}

	function uploadBanner(input) {
		if (input.files[0]) {
			var reader = new FileReader();
			var formData = new FormData();
      reader.onload = function (e) {
      	var data = {
      		'imageThumb' : e.target.result
      	}
      	$.ajax({
      		url 		: baseUrl+'banner/save_banner',
      		type 		: "POST",
      		dataType: "json",
      		data 		: data
      	}).done(function(r){
      		var html = 
    				'<input type="text" name="post-thumb" value="'+r.value+'" hidden>'
            +'<input type="file" id="choose-thumb" class="loading" style="display: none;">'
            +'<img src="'+baseUrl+r.value+'">';
            $('.banner-thumb').html(html);
            _removeButton();
      	}).fail(function(error){

      	});	
      }
      reader.readAsDataURL(input.files[0]);
    }
	}

	function readUrl(input) {
		if (input.files[0]) {
			var reader = new FileReader();
			var formData = new FormData();
      reader.onload = function (e) {
      	var data = {
      		'imageThumb' : e.target.result
      	}
      	$.ajax({
      		url 		: baseUrl+'post/save_thumb',
      		type 		: "POST",
      		dataType: "json",
      		data 		: data
      	}).done(function(r){
      		var html = 
    				'<div class="btn-remove">'
              +'<i class="fa fa-minus-circle"></i>'
        		+'</div>'
        		+'<input type="text" name="post-thumb" value="'+r.value+'" hidden>'
            +'<input type="file" id="choose-thumb" class="loading" style="display: none;">'
            +'<img src="'+baseUrl+'uploads/images/posts/new_post.jpg">';
            $('.display-thumb').html(html);
            _removeButton();
      	}).fail(function(error){

      	});	
      }
      reader.readAsDataURL(input.files[0]);
    }
	}

	function setCoverAlbum() {
		$('.check').click(function(e){
      e.preventDefault();
      var value = this.getAttribute('dataImage');
      $('[name=cover_album]').val(value);
      $('.check').addClass('btn-transparen');
      $('.check').removeClass('btn-success');
      $(this).removeClass('btn-transparen');
      $(this).addClass('btn-success');
    });
	}

	function uploadAlbum(input){
		$.each(input.files, function (k, v){
      if (input.files[k]) {
	        var reader = new FileReader();
	        var formData = new FormData();
	        reader.onload = function (e) {
          formData.append("image_data", e.target.result); 
	          $.ajax({
	            url: baseUrl+"gallery/upload",
	            type: "POST",
	            data: formData,
	            dataType: "json",
	            contentType: false,
	            processData: false
	          }).done(function(data){
	          	$(".preview-image").append(data);
              setCoverAlbum();
	          }).fail(function(){

	          });
	        }
	      reader.readAsDataURL(input.files[k]);
      }
    });
	}

	function deleteImageAlbum(){
		$('.remove').on('click', function(e){
			e.preventDefault();
			var element = this.parentNode.parentNode;
			var data = {
				'image' : this.getAttribute('dataImage')
			};
			$.ajax({
				url: baseUrl+"gallery/unset_image",
				type: "post",
				data: data,
				dataType: 'json'
			}).done(function(r){
				if (r == 1) {
					$(element).remove('.preview');
				}
			}).fail(function(){

			});
		});
	}

	function chooseThumb(){
		$('.display-thumb > img').on('click', function(){
			var choose = document.getElementById('choose-thumb');
	    choose.click();
		});
		$('.banner-thumb > img').on('click', function(){
			var choose = document.getElementById('banner');
	    choose.click();
		});
	}

	function load(){
		_removeButton();
		uploadButton();
		setCoverAlbum();
		chooseThumb();
		deleteImageAlbum();
		$('[name=fileAlbum]').on('change', function(){
			uploadAlbum(this);
		});
	}

	load();
});