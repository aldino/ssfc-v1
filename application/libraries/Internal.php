<?php
include APPPATH . "third_party/ImageResize.php";
use \Eventviva\ImageResize;
/**
* 
*/
class Internal
{

	public function randomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function printVar($value = null) {
        echo '<pre>';
        return var_dump($value);
        echo '</pre>';
    }

    public function upload($file = null) {
        return $image = ImageResize::createFromString($file);
    }

    public function thumb($file = null, $width = 0, $height = 0) {
        $image = new ImageResize($file);
        $image->crop($width, $height);
        $image->getImageAsString(IMAGETYPE_JPEG, 4);
        return $thumb = base64_encode($image);
    }

    public function uploadBanner($file = null){
        $image = ImageResize::createFromString($file);
        return $image->resize(1920, 1000);
    }

    public function uploadPostThumb($file = null) {
        $image = ImageResize::createFromString($file);
        return $image->resize(500, 334);
    }
}
?>