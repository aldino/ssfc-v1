<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add New Banner</h3>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <a href="<?=base_url()?>dashboard/banner/add" class="btn btn-primary"> Add New</a>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Banners</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Images</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($banners as $banner) : ?>
                                <tr>
                                    <td></td>
                                    <td><img src="<?=base_url() . 'uploads/images/banner/' . $banner->link_banner?>" width="400" height="200"></td>
                                    <td>
                                        <a href="<?=base_url()?>banner/delete/<?=$banner->link_banner?>">Delete</a>
                                    </td>
                                </tr>
                                <?php endforeach;  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->