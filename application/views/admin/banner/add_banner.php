<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add New Banner</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <?php
                    echo form_open("banner/save"); 
                ?>
                    <div class="x_title">
                        <h2>Banner</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <label class="control-label col-sm-2" for="first-name">
                            Post Thumbnail
                        </label>
                        <div class="col-sm-2 banner-thumb">
                            <input type="file" id="banner" class="loading" style="display: none;">
                            <img src="<?=base_url('uploads/default/default-image.jpg')?>">
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <a href="<?=base_url() . 'dashboard/banner/add'?>" class="btn btn-primary">Add New</a>
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->