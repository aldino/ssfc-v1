<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add New User</h3>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <a href="<?=base_url()?>dashboard/user/add" class="btn btn-primary"> Add New</a>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>All Users</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>User Login</th>
                                <th>User Email</th>
                                <th>First Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($users as $user) : ?>
                                <tr>
                                    <td></td>
                                    <td><a href="<?=base_url()?>dashboard/user/edit/<?=$user->ID?>"><?=$user->user_login?></a></td>
                                    <td><a href="<?=base_url()?>dashboard/user/edit/<?=$user->ID?>"><?=$user->user_email?></a></td>
                                    <td><a href="<?=base_url()?>dashboard/user/edit/<?=$user->ID?>"><?=$user->first_name?></a></td>
                                    <td>
                                        <a href="">View</a> ||
                                        <a href="<?=base_url()?>dashboard/user/edit/<?=$user->ID?>">Edit</a> ||
                                        <a href="<?=base_url()?>user/delete/<?=$user->ID?>">Delete</a>
                                    </td>
                                </tr>
                                <?php endforeach;  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->