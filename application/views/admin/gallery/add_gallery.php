<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Media</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel form-horizontal">
                <?php
                    echo form_open("gallery/add/"); 
                ?>
                    <div class="x_title">
                        <h2>Media</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Enter title here" class="form-control" name="title">
                    </div>
                    <div class="form-group">
                        <textarea id="post-content" class="form-control" name="description" placeholder="Insert description here"></textarea>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <label class="col-sm-2">Kategori</label>
                        <div class="col-md-10">
                            <select class="form-control" name="categori">
                                <option value="-"> - Pilih Kategori - </option>
                                <option value="ontheroad"> On The Road </option>
                                <option value="event"> Events </option>
                                <option value="socialisation"> Socialisation </option>
                            </select>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <label class="col-sm-2">Cover Album</label>
                        <div class="col-md-10">
                            <input type="text" name="cover_album" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <label class="col-sm-2">Upload Image</label>
                        <div class="col-md-10">
                            <label class="btn btn-primary">
                                <i class="fa fa-upload"></i>
                                Upload Image
                                <input type="file" name="fileAlbum" multiple style="display: none;">
                            </label>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="preview-image">
                            
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="submit" name="publish" class="btn btn-primary" value="Publish">
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->