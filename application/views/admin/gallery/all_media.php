page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> Media Gallery </h3>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <a href="<?=base_url()?>dashboard/gallery/add" class="btn btn-primary"> Add New</a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Media Gallery</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">
                        <?php foreach ($medias as $media) : ?>
                            <div class="col-md-55">
                                <div class="thumbnail">
                                    <div class="image view view-first">
                                        <img style="width: 100%; display: block;" src="<?=base_url('uploads/images/albums/' . $media->link_cover . '')?>" alt="image">
                                        <div class="mask">
                                            <p><?=$media->title?></p>
                                            <div class="tools tools-bottom">
                                                <a href="<?=base_url() . 'uploads/images/albums/' . $media->link_cover?>"><i class="fa fa-link"></i></a>
                                                <a href="<?=base_url() . 'dashboard/gallery/edit/' . $media->ID?>"><i class="fa fa-pencil"></i></a>
                                                <a href="<?=base_url() . "gallery/delete/" . $media->ID?>"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <?php
                                            $text = $media->description;
                                            if (strlen($text) > 53)
                                                $text = substr($text, 0, 50) . '...';
                                            echo $text;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                        <div class="row">
                            <?php 
                                echo $this->pagination->create_links();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content