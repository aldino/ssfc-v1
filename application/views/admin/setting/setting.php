<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Setting</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel form-horizontal">
                <?php
                    echo form_open("setting/save"); 
                ?>
                    <div class="x_title">
                        <h2>General</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label>Site Name</label>
                        <input type="text" placeholder="Enter title here" class="form-control" name="site_name" value="<?=$settings['site_name']?>">
                    </div>
                    <div class="form-group">
                        <label>Instagram</label>
                        <input type="text" placeholder="Enter title here" class="form-control" name="instagram" value="<?=$settings['instagram']?>">
                    </div>
                    <div class="form-group">
                        <label>Email Admin</label>
                        <input type="text" placeholder="Enter title here" class="form-control" name="admin_email" value="<?=$settings['admin_email']?>">
                    </div>
                    <div class="form-group">
                        <label>Short Name</label>
                        <input type="text" placeholder="Enter title here" class="form-control" name="short_name" value="<?=$settings['short_name']?>">
                    </div>
                    <div class="form-group">
                        <label>Facebook</label>
                        <input type="text" placeholder="Enter title here" class="form-control" name="facebook" value="<?=$settings['facebook']?>">
                    </div>
                    <div class="form-group">
                        <label>Twitter</label>
                        <input type="text" placeholder="Enter title here" class="form-control" name="twitter" value="<?=$settings['twitter']?>">
                    </div>
                    <div class="form-group">
                        <label>Sejarah SSFC Indonesia</label>
                        <textarea id="post-content" class="form-control" name="ssfc_indonesia"><?=$settings['ssfc_indonesia']?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Sejarah SSFC Indramayu</label>
                        <textarea id="ssfc-indramayu" class="form-control" name="ssfc_indramayu"><?=$settings['ssfc_indramayu']?></textarea>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="submit" name="publish" class="btn btn-primary" value="Publish">
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->