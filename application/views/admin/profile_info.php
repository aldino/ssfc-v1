<?php 

	$users = $this->session->user_loggedin;
	foreach ($users as $user) :
?>
<!-- menu profile quick info -->
<div class="profile clearfix">
    <div class="profile_pic">
        <img src="<?=$user->link_image !== '' ? $user->link_image : base_url() . 'uploads/default/default-profile.png' ?>" alt="..." class="img-circle profile_img">
    </div>
    <div class="profile_info">
        <span>Welcome,</span>
        <h2><?=$user->first_name . ' ' . $user->last_name?></h2>
    </div>
</div>
<?php endforeach; ?>
<!-- /menu profile quick info -->