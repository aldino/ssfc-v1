<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add New Menu</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <?php 
                        $attributes = [
                            'class' => 'form-horizontal form-label-left'
                        ];
                        echo form_open('menu/save', $attributes);
                    ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Menu</label>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <select class="form-control" name="menu">
                                    <option value="0">- Selected Page -</option>
                                    <?php foreach ($pages as $page) : ?>
                                        <option value="<?=$page->post_id?>"><?=$page->post_title?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Parent Menu</label>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <select class="form-control" name="parent">
                                    <option value="0">- Selected Parent -</option>
                                    <?php foreach ($menus as $menu) : ?>
                                        <option value="<?=$menu->menu_id?>"><?=$menu->post_title?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">Cancel</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Menus</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Menu Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($menus as $menu) : ?>
                                <tr>
                                    <td></td>
                                    <td><a href="<?=base_url()?>dashboard/menu/edit/<?=$menu->menu_id?>"><?=$menu->post_title?></a></td>
                                    <td>
                                        <a href="<?=base_url()?>menu/delete/<?=$menu->menu_id?>">Delete</a>
                                    </td>
                                </tr>
                                <?php endforeach;  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->