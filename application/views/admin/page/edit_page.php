<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Page</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <?php
                    foreach ($posts as $post) :
                    echo form_open("post/edit/" . $post->post_id . '/2'); 
                ?>
                    <div class="x_title">
                        <h2>Page</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <input type="text" placeholder="Enter title here" class="form-control" name="post_title" value="<?=$post->post_title?>">
                    </div>
                    <div class="x_content">
                        <textarea id="post-content" class="form-control" name="post_content"><?=$post->post_content?></textarea>
                        <div class="ln_solid"></div>
                    </div>
                    <div class="x_content">
                        <label class="control-label col-sm-2" for="first-name">
                            Post Thumbnail
                        </label>
                        <div class="col-sm-2 display-thumb">
                            <?php if ($post->post_thumb) : ?>
                                <div class="btn-remove" fileName="<?=$post->post_thumb?>">
                                    <i class="fa fa-minus-circle"></i>
                                </div>
                            <?php endif; ?>
                            <input type="file" id="choose-thumb" class="loading" style="display: none;">
                            <?php 
                                $linkUrl = $post->post_thumb ? 'uploads/images/posts/' . $post->post_thumb : 'uploads/default/default-image.jpg';
                            ?>
                            <img src="<?=base_url($linkUrl)?>">
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="submit" name="publish" class="btn btn-primary" value="Publish">
                        </div>
                    </div>
                <?php 
                    echo form_close(); 
                    endforeach;
                ?>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->