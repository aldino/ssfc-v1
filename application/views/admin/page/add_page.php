<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add New Page</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <?php
                    echo form_open("post/save/2"); 
                ?>
                    <div class="x_title">
                        <h2>Page</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                            Post Title
                        </label>
                        <input type="text" placeholder="Enter title here" class="form-control" name="post_title" value="<?=isset($posts) ? $posts['post']['post_title'] : ''?>">
                    </div>
                    <div class="x_content">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                            Post Content
                        </label>
                        <br>
                        <textarea id="post-content" class="form-control" name="post_content"><?=isset($posts) ? $posts['post']['post_content'] : ''?></textarea>
                    </div>
                    <div class="x_content">
                        <label class="control-label col-sm-2" for="first-name">
                            Post Thumbnail
                        </label>
                        <div class="col-sm-2 display-thumb">
                            <input type="file" id="choose-thumb" class="loading" style="display: none;">
                            <img src="<?=base_url('uploads/default/default-image.jpg')?>">
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="submit" name="publish" class="btn btn-primary loading" value="Publish">
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->