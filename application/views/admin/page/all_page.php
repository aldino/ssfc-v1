<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add New Page</h3>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <a href="<?=base_url()?>dashboard/page/add" class="btn btn-primary"> Add New</a>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Pages</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Page Title</th>
                                <th>Page Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($posts as $post) : ?>
                                <tr>
                                    <td></td>
                                    <td><a href="<?=base_url()?>dashboard/page/edit/<?=$post->post_id?>"><?=$post->post_title?></a></td>
                                    <td><?=$post->post_status == 1 ? 'Publish' : 'Draft'?></td>
                                    <td>
                                        <a href="">View</a> ||
                                        <a href="<?=base_url()?>dashboard/page/edit/<?=$post->post_id?>">Edit</a> ||
                                        <a href="<?=base_url()?>page/delete/<?=$post->post_id?>">Delete</a> ||
                                        <a href="<?=base_url()?>page/set_status/<?=$post->post_id?>/1">Publish</a> ||
                                        <a href="<?=base_url()?>page/set_status/<?=$post->post_id?>/2">Draft</a>
                                    </td>
                                </tr>
                                <?php endforeach;  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->