<br />
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li><a><i class="fa fa-edit"></i> Posts <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?=base_url()?>dashboard/post">All Posts</a></li>
                    <li><a href="<?=base_url()?>dashboard/post/add">Add New</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-clone" aria-hidden="true"></i> Pages <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?=base_url('dashboard/page')?>">All Pages</a></li>
                    <li><a href="<?=base_url('dashboard/page/add')?>">Add New</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-camera"></i> Gallery <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?=base_url()?>dashboard/gallery">All Album</a></li>
                    <li><a href="<?=base_url()?>dashboard/gallery/add">Add New</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?=base_url('dashboard/user')?>">All Users</a></li>
                    <li><a href="<?=base_url('dashboard/user/add')?>">Add New</a></li>
                    <li><a href="tables_dynamic.html">Your Profile</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-cog"></i> Settings <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?=base_url('dashboard/setting')?>">General</a></li>
                    <li><a href="<?=base_url('dashboard/menu')?>">Menu</a></li>
                    <li><a href="<?=base_url('dashboard/banner')?>">Banner</a></li>
                    <li><a href="morisjs.html">Team</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /sidebar menu -->