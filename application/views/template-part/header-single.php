<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$settings['site_name']?></title>
<meta name="description" content="">
<meta name="author" content="">


<!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="hiroshima liburan jepang, jasa booking online fotografer profesional di jepang, 50 free edited photos, dikirim 5 hingga 7 hari pemotretan"/>
<meta name="robots" content="noodp"/>
<link rel="canonical" href="<?=base_url('single/post/' . $posts[0]->slug)?>" />
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="<?=$posts[0]->post_title?>" />
<meta property="og:description" content="<?=strip_tags(substr($posts[0]->post_content, 0, 150))?>" />
<meta property="og:url" content="<?=base_url('single/post/' . $posts[0]->slug)?>" />
<meta property="og:site_name" content="<?=$settings['site_name']?>" />
<meta property="article:publisher" content="<?=base_url()?>" />
<meta property="article:section" content="Travelling" />
<meta property="og:image" content="<?=base_url('uploads/images/posts/' . $posts[0]->post_thumb)?>" />
<meta property="og:image:width" content="750" />
<meta property="og:image:height" content="422" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="<?=strip_tags(substr($posts[0]->post_content, 0, 150))?>" />
<meta name="twitter:title" content="<?=$posts[0]->post_title?>" />
<meta name="twitter:image" content="<?=base_url('uploads/images/posts/' . $posts[0]->post_thumb)?>" />
<!-- / Yoast SEO plugin. -->

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="<?=base_url('assets/css/bootstrap.css');?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/fonts/font-awesome/css/font-awesome.css');?>">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css"  href="<?=base_url('assets/css/style.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/nivo-lightbox/nivo-lightbox.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/nivo-lightbox/default.css');?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand page-scroll" href="#page-top"><img src="<?=base_url('assets/img/logo-ssfcindramayu.png')?>" width="135px" height="60px"></a> 
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?=base_url('#blog')?>" class="page-scroll">Blog</a></li>
        <li><a href="<?=base_url('#sejarah')?>" class="page-scroll">Sejarah</a></li>
        <li><a href="<?=base_url('#gallery')?>" class="page-scroll">Gallery</a></li>
        <li><a href="<?=base_url('#anggota')?>" class="page-scroll">Anggota</a></li>
        <li><a href="<?=base_url('#call-reservation')?>" class="page-scroll">Contact</a></li>
        <?php if (!$this->session->user_loggedin) : ?>
            <li><a href="<?=base_url('login')?>" classs="page-scroll">Login</a></li>
        <?php endif; ?>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>