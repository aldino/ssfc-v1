<!-- Team Section -->
<div id="anggota" class="text-center">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 section-title">
                    <h2>Anggota</h2>
                    <hr>
                    <p>Daftar Anggota Ssfc Korwil Indramayu Barat sudah terdaftar sebagai anggota Inti dan Partisipan :</p>
                </div>
            </div>
            <div id="row">
                <div class="swiper-container anggota">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" style="background-image:url(img/anggota/dhede.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(img/anggota/ipin.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(img/anggota/daddy.jpg)"></div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>
                </div>
            </div>
        </div>
    </div>
</div>