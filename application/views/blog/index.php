<!-- Blog Section -->
<div id="blog">
    <div class="container">
        <div class="row">
            <div class="section-title text-center">
                <h2>Berita Terbaru</h2>
                <hr>
            </div>
        </div>
        <?php foreach ($posts as $post) : ?>
            <?php 
                $image = 'uploads/images/posts/' . $post->post_thumb;
                $thumb = $this->internal->thumb($image, 500, 334);
            ?>
        <div class="row">
            <div class="col-xs-12 col-md-6 ">
                <div class="about-img"><img src="data:image/png;base64,<?=$thumb?>" class="img-responsive" alt=""></div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="about-text">
                    <h2><a href="<?=base_url('single/post/' . $post->slug)?>"><?=$post->post_title?></a></h2>
                    <p><i class="fa fa-calendar"></i> <?=date('d M Y H:i:s', strtotime($post->post_date))?></p>
                    <hr>
                    <?=substr($post->post_content, 0,250)?>
                </div>
            </div>
        </div>
        <div class="cleaner"></div>
        <?php endforeach; ?>
        <div class="row">
            <div class="section-title text-center">
                <h4><a href="<?=base_url('blog')?>">Lihat semua berita..</a></h4>
                <hr>
            </div>
        </div>
    </div>
</div>
<!-- Sejarah Section -->
<div id="sejarah">
    <div class="section-title text-center center">
        <div class="overlay">
            <h2>Sejarah</h2>
            <hr>
            <p>SSFC Indonesia & SSFC Korwil Indramayu Barat</p>
        </div>
    </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="menu-section">
          <h2 class="menu-section-title">SSFC Indonesia</h2>
          <hr>
          <div class="menu-item">
              <?php echo $settings['ssfc_indonesia']; ?>
          </div>
        </div>
      </div>
        <div class="col-xs-12 col-sm-6">
            <div class="menu-section">
                <h2 class="menu-section-title">SSFC Korwil Indramayu Barat</h2>
                <hr>
                <div class="menu-item">
                    <?php echo $settings['ssfc_indramayu']; ?>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<!-- Gallery Section -->
<div id="gallery">
    <div class="section-title text-center center">
        <div class="overlay">
            <h2>Gallery</h2>
            <hr>
            <p>We are SSFC Indramayu Raya</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="categories">
                <ul class="cat">
                    <li>
                        <ol class="type">
                            <li><a href="#" data-filter="*" class="active">All</a></li>
                            <li><a href="#" data-filter=".ontheroad">On the road</a></li>
                            <li><a href="#" data-filter=".event">Events</a></li>
                            <li><a href="#" data-filter=".socialisation">Socialisation</a></li>
                        </ol>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="portfolio-items">
                <?php 
                    foreach ($galleries as $gallery) : 
                    $image = 'uploads/images/albums/' . $gallery->link_cover;
                    $albums = $this->internal->thumb($image, 500, 333);
                ?>
                <div class="col-sm-6 col-md-4 col-lg-4 <?=$gallery->category?>">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="<?=base_url('single/gallery/' . $gallery->ID)?>" title="<?=$gallery->title?>">
                            <div class="hover-text">
                                <h4><?=$gallery->title?></h4>
                            </div>
                            <img src="data:image/png;base64,<?=$albums?>" class="img-responsive" alt="Project Title"> </a> 
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<!-- Team Section -->
<div id="anggota" class="text-center">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 section-title">
                    <h2>Anggota</h2>
                    <hr>
                    <p>Daftar Anggota Ssfc Korwil Indramayu Barat sudah terdaftar sebagai anggota Inti dan Partisipan :</p>
                </div>
            </div>
            <div id="row">
                <div class="swiper-container anggota">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" style="background-image:url(uploads/images/anggota/dhede.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(uploads/images/anggota/ipin.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(uploads/images/anggota/daddy.jpg)"></div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Call Reservation Section -->
<!-- <div id="call-reservation" class="text-center">
    <div class="container">
        <h2>Want to make a reservation? Call <strong>1-887-654-3210</strong></h2>
    </div>
</div> -->
<!-- Contact Section -->
<div id="contact" class="text-center">
    <div class="container">
        <div class="section-title text-center">
            <h2>Contact Form</h2>
            <hr>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit duis sed.</p>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <form name="sentMessage" id="contactForm" novalidate>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" id="name" class="form-control" placeholder="Name" required="required">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="email" id="email" class="form-control" placeholder="Email" required="required">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <textarea name="message" id="message" class="form-control" rows="4" placeholder="Message" required></textarea>
                <p class="help-block text-danger"></p>
            </div>
            <div id="success"></div>
            <button type="submit" class="btn btn-custom btn-lg">Send Message</button>
            </form>
        </div>
    </div>
</div>