<div id="footer">
  <div class="container text-center">
    <div class="col-md-4">
      <h3>Alamat</h3>
      <div class="contact-item">
        <p>Jl. Raya Karangsinom - Gabus,</p>
        <p>Selatan Alfamart Karangsinom samping Warkop.</p>
      </div>
    </div>
    <div class="col-md-4">
      <h3>Kopi Darat</h3>
      <div class="contact-item">
        <p>Sabtu: 20:00 Wib - Selesai</p>
      </div>
    </div>
    <div class="col-md-4">
      <h3>Contact Info</h3>
      <div class="contact-item">
        <p>Phone: +6289695464262</p>
        <p>Email: info@ssfckorwilibrt.or.id</p>
      </div>
    </div>
  </div>
  <div class="container-fluid text-center copyrights">
    <div class="col-md-8 col-md-offset-2">
      <div class="social">
        <ul>
          <li><a href="<?=$settings['facebook']?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="<?=$settings['twitter']?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="<?=$settings['instagram']?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
      <p>&copy; <?=$settings['site_name']?></p>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?=base_url('assets/js/jquery.1.11.1.js');?>"></script> 
<script type="text/javascript" src="<?=base_url('assets/js/bootstrap.js');?>"></script> 
<script type="text/javascript" src="<?=base_url('assets/js/SmoothScroll.js');?>"></script> 
<script type="text/javascript" src="<?=base_url('assets/js/nivo-lightbox.js');?>"></script> 
<script type="text/javascript" src="<?=base_url('assets/js/jquery.isotope.js');?>"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>
<script type="text/javascript" src="<?=base_url('assets/js/jqBootstrapValidation.js');?>"></script> 
<script type="text/javascript" src="<?=base_url('assets/js/contact_me.js');?>"></script> 
<script type="text/javascript" src="<?=base_url('assets/js/main.js');?>"></script>
</body>
</html>