<?php foreach ($galleries as $gallery) : ?>
<div id="gallery">
    <div class="section-title text-center center">
        <div class="overlay">
            <h2><?=$gallery->title?></h2>
            <hr>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="portfolio-items">
                <?php 
                    foreach ($det_galleries as $detgallery) : 
                    $image = base_url('uploads/images/albums/' . $detgallery->link);
                    $albums = $this->internal->thumb($image, 500, 333);
                ?>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="<?=$image?>" title="<?=$gallery->title?>" data-lightbox-gallery="gallery1">
                            <div class="hover-text">
                                <h4><?=$gallery->title?></h4>
                            </div>
                            <img src="data:image/png;base64,<?=$albums?>" class="img-responsive" alt="Project Title"> </a> 
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>