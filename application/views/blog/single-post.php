<?php foreach ($posts as $post) : ?>
<div id="single-post">
	<div class="row">
		<div class="section-title text-center center">
	        <div class="overlay">
	            <h2><?=$post->post_title?></h2>
	            <hr>
	        </div>
	    </div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<div class="post-thumb">
						<img src="<?=base_url('uploads/images/posts/' . $post->post_thumb)?>">
					</div>
					<div class="cleaner"></div>
					<div class="row">
						<?=$post->post_content?>	
					</div>
					<div class="cleaner"></div>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
</div>
<?php endforeach; ?>