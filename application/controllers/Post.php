<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(['post_model', 'setting_model']);
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library(['form_validation', 'session', 'internal']);
        if (!$this->session->user_loggedin) {
            redirect(base_url('dashboard'));
        }
	}

	public function index() {
		
	}
	
	public function save($postType = 1) 
	{
        $this->post_model->setPost(0,$postType);
        $redirect = $postType == 1 ? base_url() . 'dashboard/post' : base_url() . 'dashboard/page';
        redirect($redirect);
	}

	public function save_thumb() {
		$imageThumb = $this->input->post('imageThumb');
	    $search = [
	    	'data:image/png;base64,',
	    	'data:image/jpg;base64,',
	    	'data:image/jpeg;base64,',
	    	'data:image/gif;base64,'
	    ];
	    $find = [
	    	'',
	    	'',
	    	'',
	    	''
	    ];
	    $imageThumb = str_replace($search, $find, $imageThumb);
	    $imageThumb = str_replace(' ', '+', $imageThumb);
	    $data = base64_decode($imageThumb);
	    $file = 'uploads/images/posts/new_post.jpg';
	    $success = file_put_contents($file, $data);
	    $result = [
	    	'result' => true,
	    	'value'	 => $file
	    ];
	    echo json_encode($result);
	}

	public function delete_thumb($thumb = null) {
		$this->post_model->deleteThumb($thumb);
		echo "true";
	}

	public function set_status($id = 0, $val = 0) {
		$this->post_model->setStatus($id, $val);
		redirect(base_url() . 'dashboard/post/');
	}

	public function edit($id = 0, $postType = 1) {
        $this->post_model->setPost($id, $postType);
        $redirect = $postType == 1 ? base_url() . 'dashboard/post' : base_url() . 'dashboard/page';
        redirect($redirect);
	}

	public function delete($id = 0) {
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library('form_validation');
        $this->post_model->deletePost($id);
        redirect(base_url() . 'dashboard/post');
	}
}
