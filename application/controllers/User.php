<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library(['form_validation', 'session']);
        if (!$this->session->user_loggedin) {
            redirect(base_url('dashboard'));
        }
	}

	public function save() 
	{
        $this->user_model->setUser();
        redirect(base_url() . 'dashboard/user');
	}

	public function edit($id = 0) {
	    $this->user_model->setUser($id);
        redirect(base_url() . 'dashboard/user');
	}

	public function delete($id) {
	    $this->user_model->deleteUser($id);
        redirect(base_url() . 'dashboard/user');
	}
}
