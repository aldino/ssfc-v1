<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(['setting_model', 'banner_model']);
		$this->load->library(['session', 'internal']);
		$this->load->helper('url');
		if (!$this->session->user_loggedin) {
            redirect(base_url('dashboard'));
        }
	}

	public function save_banner() 
	{

		$imageThumb = $this->input->post('imageThumb');
	    $search = [
	    	'data:image/png;base64,',
	    	'data:image/jpg;base64,',
	    	'data:image/jpeg;base64,',
	    	'data:image/gif;base64,'
	    ];
	    $find = [
	    	'',
	    	'',
	    	'',
	    	''
	    ];
	    $imageThumb = str_replace($search, $find, $imageThumb);
	    $imageThumb = str_replace(' ', '+', $imageThumb);
	    $data = base64_decode($imageThumb);
	    $newName = $this->internal->randomString(10) . '.jpg';
	    $file = 'uploads/images/banner/' . $newName;
	    $this->internal->uploadBanner($data)
	    			   ->save($file);
	    $this->banner_model->save($newName);
	    // $success = file_put_contents($file, $data);
	    $result = [
	    	'result' => true,
	    	'value'	 => $file
	    ];
	    echo json_encode($result);
	}

	public function delete($file = null) {
		$this->banner_model->delete($file);
		redirect(base_url('dashboard/banner'));
	}
}
