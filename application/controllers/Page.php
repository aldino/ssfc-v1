<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('page_model');
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library(['form_validation']);
		if (!$this->session->user_loggedin) {
            redirect(base_url('dashboard'));
        }
	}

	public function save() 
	{
		$this->page_model->setPage();
        redirect(base_url() . 'dashboard/page');
	}

	public function edit($id = 0) {
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library('form_validation');
        $this->page_model->setPost($id);
        redirect(base_url() . 'dashboard/page');
	}

	public function set_status($id = 0, $val = 0) {
		$this->page_model->setStatus($id, $val);
		redirect(base_url() . 'dashboard/page/');
	}

	public function delete($id = 0) {
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library('form_validation');
        $this->page_model->deletePage($id);
        redirect(base_url() . 'dashboard/page');
	}
}
