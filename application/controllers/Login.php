<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library(['form_validation', 'session']);
		$this->load->model(['login_model', 'setting_model']);
	}

	public function index() {
		$data['settings'] = $this->setting_model->getSetting();
		$this->load->view('template-part/header-login', $data);
		$this->load->view('login.php', $data);
		$this->load->view('template-part/footer-login');
	}

	public function check() 
	{
		$check = $this->login_model->check();
		$this->session->set_userdata('user_loggedin', $check);
        redirect(base_url() . 'dashboard');
	}

	public function logout() {
		$this->session->unset_userdata('user_loggedin', '');
		redirect(base_url('dashboard'));
	}

	public function edit($id = 0) {
	    $this->menu_model->setMenu($id);
        redirect(base_url() . 'dashboard/menu');
	}

	public function delete($id = 0) {
        $this->menu_model->deleteMenu($id);
        redirect(base_url() . 'dashboard/menu');
	}
}
