<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('gallery_model');
		$this->load->helper(['form', 'url', 'file']);
        $this->load->library(['internal', 'session']);
        if (!$this->session->user_loggedin) {
            redirect(base_url('dashboard'));
        }
	}

	public function upload() 
	{
		$image = $this->input->post('image_data');
        $search = [
            'data:image/png;base64,',
            'data:image/jpg;base64,',
            'data:image/jpeg;base64,',
            'data:image/gif;base64,'
        ];
        $find = [
            '',
            '',
            '',
            ''
        ];
        $image = str_replace($search, $find, $image);
        $image = str_replace(' ', '+', $image);
        $data = base64_decode($image);
        $newName = $this->internal->randomString(20) . '.jpg';
        $file = 'uploads/images/albums/' . $newName;
        $success = file_put_contents($file, $data);
        $html = '
            <div class="preview">
                <div class="icon-group">
                    <a href="#" class="btn btn-danger remove" dataimage="' . $newName . '"><i class="fa fa-remove"></i></a>
                    <a href="#" class="btn btn-transparen check" dataimage="' . $newName . '"><i class="fa fa-check"></i></a>
                </div>
                <input type="hidden" name="image[]" value="' . $newName . '">
                <img src="' . base_url() . $file . '">
            </div>
        ';
        echo json_encode($html);
	}

    public function add() {
        $this->gallery_model->setGallery();
        redirect(base_url() . 'dashboard/gallery');
    }

    public function unset_image() {
        $image = $this->gallery_model->unsetImage();
        echo $image;
    }

	public function edit($id = 0) {
        $this->gallery_model->setGallery($id);       
        redirect(base_url() . 'dashboard/gallery/edit/' . $id);
	}

	public function delete($id = 0) {
        $this->gallery_model->deleteGallery($id);
        redirect(base_url() . 'dashboard/gallery');
	}
}
