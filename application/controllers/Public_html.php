<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Public_html extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(['setting_model', 'banner_model', 'post_model', 'gallery_model']);
		$this->load->library(['session', 'internal']);
		$this->load->helper('url');
	}

	public function index($slug = null)
	{
		$args = [
			'post_type' => 1,
			'post_status' => 1
		];
		$data['posts'] = $this->post_model->getPostsPublish($args, 3, 0);
		$data['banners'] = $this->banner_model->getBanners();
		$data['settings'] = $this->setting_model->getSetting();
		$data['galleries'] = $this->gallery_model->getMedia();
		$this->load->view('blog/header', $data);
		$this->load->view('blog/banner', $data);
		$this->load->view('blog/index', $data);
		$this->load->view('blog/footer', $data);
	}
}
