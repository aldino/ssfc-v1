<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Single extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(['post_model', 'setting_model', 'gallery_model']);
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library(['form_validation', 'session', 'internal']);
	}

	public function post($slug = null) {
		$data['settings'] = $this->setting_model->getSetting();
		$data['posts'] = $this->post_model->getPostBySlug($slug);
		$this->load->view('template-part/header-single', $data);
		$this->load->view('blog/single-post', $data);
		$this->load->view('blog/footer', $data);
	}

	public function gallery($id = 0) {
		$data['galleries'] = $this->gallery_model->getGalleryById($id);
		$data['det_galleries'] = $this->gallery_model->getDetGalleryById($id);
		$data['settings'] = $this->setting_model->getSetting();
		$this->load->view('template-part/header-gallery', $data);
		$this->load->view('blog/single-gallery', $data);
		$this->load->view('blog/footer', $data);
	}
}
