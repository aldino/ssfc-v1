<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library(['form_validation', 'session']);
		$this->load->model('menu_model');
		if (!$this->session->user_loggedin) {
            redirect(base_url('dashboard'));
        }
	}

	public function save() 
	{
        $this->menu_model->setMenu();
        redirect(base_url() . 'dashboard/menu');
	}

	public function edit($id = 0) {
	    $this->menu_model->setMenu($id);
        redirect(base_url() . 'dashboard/menu');
	}

	public function delete($id = 0) {
        $this->menu_model->deleteMenu($id);
        redirect(base_url() . 'dashboard/menu');
	}
}
