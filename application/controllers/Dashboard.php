<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model(['post_model', 'user_model', 'gallery_model', 'page_model', 'menu_model', 'setting_model', 'banner_model']);
		$this->load->helper(['url', 'form']);
		$this->load->library(['pagination', 'internal', 'session']);
		if (!$this->session->user_loggedin) {
			redirect(base_url('login'));
		}
	}

	public function index()
	{
		$data['settings'] = $this->setting_model->getSetting();
		$this->load->view('admin/header', $data);
		$this->load->view('admin/profile_info');
		$this->load->view('admin/sidebar_menu');
		$this->load->view('admin/menu_footer_button');
		$this->load->view('admin/top_navigation');
		// $this->load->view('admin/post/form_post');
		$this->load->view('admin/footer');
	}

	public function post($action = '', $id = 0) 
	{
		$data['settings'] = $this->setting_model->getSetting();
		if ($action === 'add') {
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/post/add_post');
			$this->load->view('admin/footer');
		} elseif ($action === 'edit') {
			$data['posts'] = $this->post_model->getPostById($id);
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/post/edit_post', $data);
			$this->load->view('admin/footer');
		} else {
			if ($this->session->user_loggedin[0]->user_auth == 1) {
				$data['posts'] = $this->post_model->getPost();
			} else {
				$data['posts'] = $this->post_model->getPostByAuthor($this->session->user_loggedin[0]->ID);
			}
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/post/all_post', $data);
			$this->load->view('admin/footer'); 
		}
	}

	public function page($action = '', $id = 0) 
	{
		$data['settings'] = $this->setting_model->getSetting();
		if ($action === 'add') {
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/page/add_page');
			$this->load->view('admin/footer');
		} elseif ($action === 'edit') {
			$data['posts'] = $this->page_model->getPageById($id);
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/page/edit_page', $data);
			$this->load->view('admin/footer');
		} else {
			$data['posts'] = $this->page_model->getPage();
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/page/all_page', $data);
			$this->load->view('admin/footer'); 
		}
	}

	public function gallery($gallery = '', $id = 0) {
		$data['settings'] = $this->setting_model->getSetting();
		if ($gallery === 'add') {
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/gallery/add_gallery.php');
			$this->load->view('admin/footer');
		} elseif ($gallery === 'edit') {
			$data['gallerys']  = $this->gallery_model->getGalleryById($id);
			$data['images']	   = $this->gallery_model->getImageById($id);
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/gallery/edit_gallery', $data);
			$this->load->view('admin/footer');
		} else {
			$count = $this->gallery_model->galleryCount();
			$config['base_url']		= base_url() . 'dashboard/gallery';
			$config['total_rows']	= $count;
			$config['per_page']		= 10;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";
			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);
			$data['medias']		= $this->gallery_model->data($config['per_page'], $from);
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/gallery/all_media', $data);
			$this->load->view('admin/footer');
		}
	}

	public function user($action = '', $id = 0) {
		$data['users'] = $this->user_model->getUsers();
		$data['settings'] = $this->setting_model->getSetting();
		if ($action === 'add') {
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/user/add_user');
			$this->load->view('admin/footer');
		} elseif ($action === 'edit') {
			$data['user']  = $this->user_model->getUserById($id);
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/user/edit_user', $data);
			$this->load->view('admin/footer');
		} else {
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/user/table_user', $data);
			$this->load->view('admin/footer'); 
		}
	}

	public function setting() {
		$data['settings'] = $this->setting_model->getSetting();
		$this->load->view('admin/header', $data);
		$this->load->view('admin/profile_info');
		$this->load->view('admin/sidebar_menu');
		$this->load->view('admin/menu_footer_button');
		$this->load->view('admin/top_navigation');
		$this->load->view('admin/setting/setting', $data);
		$this->load->view('admin/footer');
	}

	public function menu() {
		$data['pages'] = $this->menu_model->getPage();
		$data['menus'] = $this->menu_model->getMenus();
		$data['settings'] = $this->setting_model->getSetting();
		$this->load->view('admin/header', $data);
		$this->load->view('admin/profile_info');
		$this->load->view('admin/sidebar_menu');
		$this->load->view('admin/menu_footer_button');
		$this->load->view('admin/top_navigation', $data);
		$this->load->view('admin/menu/menu');
		$this->load->view('admin/footer'); 
	}

	public function banner($action = null) {
		$data['settings'] = $this->setting_model->getSetting();
		if ($action === 'add') {
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/banner/add_banner');
			$this->load->view('admin/footer');
		} elseif ($action === 'edit') {
			// $data['posts'] = $this->page_model->getPageById($id);
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/banner/edit_banner', $data);
			$this->load->view('admin/footer');
		} else {
			$data['banners'] = $this->banner_model->getBanners();
			$this->load->view('admin/header', $data);
			$this->load->view('admin/profile_info');
			$this->load->view('admin/sidebar_menu');
			$this->load->view('admin/menu_footer_button');
			$this->load->view('admin/top_navigation');
			$this->load->view('admin/banner/all_banner', $data);
			$this->load->view('admin/footer'); 
		}
	}
}
