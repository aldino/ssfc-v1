<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model(['setting_model', 'banner_model', 'post_model']);
		$this->load->library(['session', 'internal', 'pagination']);
		$this->load->helper('url');
	}

	public function index()
	{
		redirect(base_url('blog/page'));
	}

	public function page() {
		$args = [
			'post_type' => 1,
			'post_status' => 1
		];

		$count = $this->post_model->postCount($args);
		$config['base_url']		= base_url('blog/page');
		$config['total_rows']	= $count;
		$config['per_page']		= 2;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data['posts'] = $this->post_model->getPostsPublish($args, $config['per_page'], $from);
		$data['banners'] = $this->banner_model->getBanners();
		$data['settings'] = $this->setting_model->getSetting();
		$this->load->view('blog/header', $data);
		$this->load->view('blog/banner', $data);
		$this->load->view('blog/posts', $data);
		$this->load->view('blog/footer', $data);
	}
}
