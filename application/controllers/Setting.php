<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('setting_model');
		$this->load->library('session');
		if (!$this->session->user_loggedin) {
            redirect(base_url('dashboard'));
        }
	}

	public function save() 
	{
		$this->load->helper(['form', 'url', 'date']);
        $this->load->library('form_validation');
        $this->setting_model->setSetting();
        redirect(base_url() . 'dashboard/setting');
	}
}
