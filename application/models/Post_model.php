<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library(['form_validation', 'session', 'internal']);
	}

	public function setPost($id = 0, $postType = 1) 
	{
		$data = [];
		$postTitle 	 	= $this->input->post('post_title');
		$postContent 	= $this->input->post('post_content');
		$oldName 		= $this->input->post('post-thumb');
		$lowerPostTitle = str_ireplace(' ', '-', (strtolower($postTitle)));
		$newName 		= $lowerPostTitle . '.jpg'; 
		$renameFile 	= rename($oldName, 'uploads/images/posts/' . $newName);
		$userId 		= $this->session->user_loggedin[0]->ID;
		$slug 			= strtolower(str_ireplace(' ', '-', $postTitle));

		if (!empty($oldName)) {
			$data = [
				'post_title'	=> $postTitle,
				'post_content'	=> $postContent,
				'post_type'		=> $postType,
				'post_thumb'	=> $newName,
				'post_status'	=> 2,
				'user_id'		=> $userId,
				'slug'			=> $slug
			];
		} else {
			$data = [
				'post_title'	=> $postTitle,
				'post_content'	=> $postContent,
				'post_type'		=> $postType,
				'post_status'	=> 2,
				'user_id'		=> $userId,
				'slug'			=> $slug
			];
		}

		if ($id===0) {
			$data['post_date'] = date('Y-m-d h:i:s', now('Asia/Jakarta'));
			$this->db->insert('post', $data);
		} else {
			$this->db->where('post_id', $id);
			return $this->db->update('post', $data);
		}
	}

	public function getPostBySlug($slug = null) {
		$where = [
			'post_type' => 1,
			'slug' => $slug
		];

		$this->db->where($where);
		$post = $this->db->get('post');
		return $post->result();
	}

	public function setStatus($id = 0, $val = 0) {
		$data = [
			'post_status' => $val
		];

		$this->db->where('post_id', $id);
		return $this->db->update('post', $data);
	}

	public function deleteThumb($thumb = null) {
		$data = [
			'post_thumb' => ''
		];
		unlink('uploads/images/posts/' . $thumb);
		$this->db->where('post_thumb', $thumb);
		return $this->db->update('post', $data);
	}

	public function getPost() {
		$this->db->where('post_type', 1);
		$query = $this->db->get('post');
		return $query->result();	
	}

	public function getPostsPublish($args = null, $limit = 1, $offset = 0) {
		$this->db->where($args);
		$this->db->order_by('post_date', 'DESC');
		$query = $this->db->get('post', $limit, $offset);
		return $query->result();
	}

	public function getPostByAuthor($userId = 0) {
		$where = [
			'post_type' => 1,
			'user_id'	=> $userId
		];
		$this->db->where($where);
		$query = $this->db->get('post');
		return $query->result();
	}

	public function getPostById($id = 0) {
		$query = $this->db->get_where('post', ['post_id' => $id]);
		return $query->result();
	}

	public function deletePost($id = 0) {
		$this->db->where('post_id' , $id);
		return $this->db->delete('post');
	}

	public function postCount($args = null) {
		$this->db->where($args);
		return $this->db->get('post')->num_rows();
	}
}
