<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('internal');
	}

	public function getSetting() {
		$query = $this->db->get('settings')->result();

		foreach ($query as $setting) {
			$data[$setting->setting_name] =  $setting->value;
		}

		return $data;	
	}

	public function setSetting() {

		$settings = [
			'site_name' 	 => $this->input->post('site_name'),
			'instagram' 	 => $this->input->post('instagram'),
			'admin_email' 	 => $this->input->post('admin_email'),
			'short_name'  	 => $this->input->post('short_name'),
			'ssfc_indonesia' => $this->input->post('ssfc_indonesia'),
			'ssfc_indramayu' => $this->input->post('ssfc_indramayu'),
			'facebook'		 => $this->input->post('facebook'),
			'twitter'		 => $this->input->post('twitter')
		];
		foreach ($settings as $key => $value) {
			$data = [
				'setting_name' 	=> $key,
				'value' 		=> $value 
			];
			$this->db->where('setting_name', $key);
			$this->db->update('settings', $data);
		}
	}
}
