<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getPage() {
		$query = $this->db->get_where('post', [
				'post_type'		=> 2
			]);
		return $query->result();	
	}

	public function getPageById($id = 0) {
		$query = $this->db->get_where('post', [
				'post_id'	=> $id,
				'post_type' => 2
			]);
		return $query->result();
	}

	public function setStatus($id = 0, $val = 0) {
		$data = [
			'post_status' => $val
		];

		$this->db->where('post_id', $id);
		return $this->db->update('post', $data);
	}

	public function deletePage($id = 0) {
		$this->db->where('post_id' , $id);
		return $this->db->delete('post');
	}
}
