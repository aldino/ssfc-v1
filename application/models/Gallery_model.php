<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function setGallery($id = 0) 
	{
		$title       = $this->input->post('title');
        $description = $this->input->post('description');
        $coverAlbum  = $this->input->post('cover_album');
        $images      = $this->input->post('image');
        $category    = $this->input->post('categori');
        $date 		 = date("Y-m-d H:i:s");
        $data = [
        	'title'			=> $title,
        	'description'	=> $description,
        	'date_create'	=> $date,
        	'link_cover'	=> $coverAlbum,
        	'category'		=> $category
        ];

        if ($id==0) {
        	$this->db->insert('media', $data);
	        $mediaId = $this->db->insert_id();
	        foreach ($images as $key => $image) {
	        	$dataMedia = [
	        		'media_id' => $mediaId,
	        		'link'	   => $image
	        	];
	        	$this->db->insert('det_media', $dataMedia);
	        }
	        return true;
        } else {
        	$this->db->where('ID', $id);
        	$this->db->update("media", $data);
        	foreach ($images as $key => $image) {
        		$query = $this->db->query("select * from det_media where link='" . $image . "'");
        		if ($query->num_rows() == 0) {
        			$dataMedia = [
		        		'media_id' => $id,
		        		'link'	   => $image
		        	];
		        	$this->db->insert('det_media', $dataMedia);
        		}
        	}
        }
	}

	public function unsetImage($imageLink = null){
		$image = $imageLink !== null ? $imageLink : $this->input->post('image');
		$this->db->where('link', $image);
		$delete = $this->db->delete('det_media');
		if ($delete){
			$remove = unlink('uploads/images/albums/' . $image);
			if ($remove) {
				return true;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function getMedia() {
		$query = $this->db->get('media');
		return $query->result();	
	}

	public function getGalleryById($id = 0) {
		$query = $this->db->get_where('media', ['ID' => $id]);
		return $query->result();
	}

	public function data($limit, $page) {
		$query = $this->db->get('media', $limit, $page);
		return $query->result();
	}

	public function getImageById($id = 0) {
		$this->db->where('media_id', $id);
		return $this->db->get('det_media')->result();	
	}

	public function getDetGalleryById($id = 0) {
		$this->db->where('media_id', $id);
		return $this->db->get('det_media')->result();
	}

	public function deleteGallery($id = 0) {
		$medias = $this->getDetGalleryById($id);
		foreach ($medias as $media) {
			$delete = unlink('uploads/images/albums/' . $media->link);
			$this->db->where('media_id', $id);
			$this->db->delete('det_media');
		}

		$this->db->where('ID' , $id);
		$this->db->delete('media');
	}

	public function galleryCount() {
		return $this->db->get('media')->num_rows();
	}
}
