<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	protected $username;
	protected $password;

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	protected function setMenu($id = 0) 
	{
		
	}

	public function check() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where['user_login'] = $username;
		$where['user_pass']	= md5($password);

		$this->db->where($where);
		$query = $this->db->get('users');
		return $query->result();	
	}

	public function getMenus() {

	}

	public function getPageById($id = 0) {

	}

	public function deleteMenu($id = 0) {

	}
}
