<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('internal');
	}

	public function save($newName = null) {
		$data = [
			'link_banner' => $newName
		];
		return	$this->db->insert('banners', $data);
	}

	public function getBanners() {
		return $this->db->get('banners')->result();
	}

	public function delete($file = null) {
		$delete = unlink('uploads/images/banner/' . $file);
		if ($delete) {
			$this->db->where('link_banner', $file);
			return $this->db->delete('banners');
		}
	}

}
