<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library(['internal']);
	}

	public function setUser($id = 0) 
	{
		$userActivation = $this->internal->randomString(20);
		$password = $this->input->post('password');
		if ($password) {
			$data = [
				'user_login'			=> strtolower($this->input->post('user_login')),
				'user_email'			=> $this->input->post('email'),
				'user_pass'				=> md5($password),
				'first_name'			=> $this->input->post('first_name'),
				'last_name'				=> $this->input->post('last_name'),
				'user_registered'		=> date('Y-m-d h:i:s', now('Asia/Jakarta')),
				'user_activation_key'	=> $userActivation,
				'user_auth'				=> $this->input->post('user_auth'),
				'barcode'				=> $this->input->post('barcode')
			];
		} else {
			$data = [
				'user_login'			=> strtolower($this->input->post('user_login')),
				'user_email'			=> $this->input->post('email'),
				'first_name'			=> $this->input->post('first_name'),
				'last_name'				=> $this->input->post('last_name'),
				'user_registered'		=> date('Y-m-d h:i:s', now('Asia/Jakarta')),
				'user_activation_key'	=> $userActivation,
				'user_auth'				=> $this->input->post('user_auth'),
				'barcode'				=> $this->input->post('barcode')
			];
		}

		if ($id===0) {
			$this->db->insert('users', $data);
		} else {
			$this->db->where('ID', $id);
			return $this->db->update('users', $data);
		}
	}

	public function getUsers() {
		$query = $this->db->get('users');
		return $query->result();	
	}

	public function getUserById($id = 0) {
		$query = $this->db->get_where('users', ['ID' => $id]);
		return $query->result();
	}

	public function deleteUser($id = 0) {
		$this->db->where('ID' , $id);
		return $this->db->delete('users');
	}
}
