<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function setMenu($id = 0) 
	{
		$data = [
			'post_id'		=> $this->input->post('menu'),
			'parent_id'		=> $this->input->post('parent'),
			'menu_link'		=> ''
		];

		if ($id===0) {
			$this->db->insert('menu', $data);
		} else {
			$this->db->where('menu_id', $id);
			return $this->db->update('menu', $data);
		}
	}

	public function getPage() {
		$query = $this->db->get_where('post', [
				'post_type'		=> 2
			]);
		return $query->result();	
	}

	public function getMenus() {
		$query = $this->db->query('select M.menu_id, P.post_title, M.parent_id, M.menu_link from menu M, post P where M.post_id=P.post_id and P.post_type=2');
		return $query->result();	
	}

	public function getPageById($id = 0) {
		$query = $this->db->get_where('post', [
				'post_id'	=> $id,
				'post_type' => 2
			]);
		return $query->result();
	}

	public function deleteMenu($id = 0) {
		$this->db->where('menu_id' , $id);
		return $this->db->delete('menu');
	}
}
